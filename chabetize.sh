#!/bin/bash
mkdir chabOS
cd chabOS
wget https://www.dropbox.com/s/3a2o63o3cf9a6ia/ChabOS-Artwork.tar.gz?dl=0
tar xvpf ChabOS-Artwork.tar.gz\?dl\=0 
cd ChabOS-Artwork
sudo mkdir /usr/share/backgrounds/chabos
cp C*.png /usr/share/backgrounds/chabos
unzip AD-Tangerine-Numix_1.4.11.zip 
unzip AD-Tangerine_1.4.11.zip
sudo mv AD-Tangerine-Numix /usr/share/icons
sudo mv AD-Tangerine /usr/share/themes
unzip AD-Tangerine-Suru_1.4.11.zip
cd
rm -r chabOS
